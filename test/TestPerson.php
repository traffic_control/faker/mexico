<?php

namespace Faker\Test\Mexico;

class TestPerson extends \Faker\Mexico\Person
{
    public function removeAccents($name)
    {
        return parent::removeAccents($name);
    }
}
