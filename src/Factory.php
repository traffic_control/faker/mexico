<?php

declare(strict_types=1);

namespace Faker\Mexico;

use Faker\Generator;

class Factory extends \Faker\Factory
{
    public static function mexico(): Generator
    {
        $generator = new Generator();

        foreach (static::$defaultProviders as $provider) {
            $providerClassName = static::getProviderClassname($provider);
            $generator->addProvider(new $providerClassName($generator));
        }

        return $generator;
    }

    /**
     * {@inheritDoc}
     */
    protected static function getProviderClassname($provider, $locale = 'es_MX')
    {
        $providerClass = 'Faker\\Mexico\\' . $provider;

        if (class_exists($providerClass, true)) {
            return $providerClass;
        }

        return parent::getProviderClassname($provider, $locale);
    }
}
